import React from 'react';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';
import ParserContainer from './parser/ParserContainer.react';
import AboutContainer from './about/AboutContainer.react';
import NavigationBar from './navigation/navigationbar.react';
import ContributeContainer from './contribute/ContributeContainer.react';
import ParserTagBox from './parser/ParserTagBox.react';


export class AppRoutes extends React.Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route path={"/"} component={NavigationBar} >
                <Route path={"/parser"} component={ParserContainer} />
                <Route path={"/about"} component={AboutContainer} />
                <Route path={"/contribute"} component={ContributeContainer} />
                <Route path={"/tag"} component={ParserTagBox} />
                    <IndexRoute component={ParserContainer} />
                </Route>
            </Router>
        );
    }
}
