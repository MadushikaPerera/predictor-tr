import React from "react";
import ParserContainer from './parser/ParserContainer.react';
import AboutContainer from './about/AboutContainer.react';

export default class MyApp extends React.Component{

      render(){
        return(
          <div>
            <ParserContainer/>
            <AboutContainer/>
          </div>
        );
      }

}
